<?php

use App\Http\Controllers\Site\BlogController;
use App\Http\Controllers\Site\CategoryController;
use App\Http\Controllers\Site\ContactController;
use App\Http\Controllers\Site\HomeController;
use Illuminate\Support\Facades\Route;

Route::get('/', HomeController::class)->name('home.index');
Route::get('/blog', BlogController::class)->name('blog.index');

Route::get('/produtos', [CategoryController::class, 'index'])->name('produtos.index');
Route::get('/produtos/{slug}', [CategoryController::class, 'show'])->name('produtos.show');

Route::view('sobre', 'site.about.index')->name('sobre.index');

Route::get('contato', [ContactController::class, 'index'])->name('contato.index');
Route::post('contato', [ContactController::class, 'form'])->name('contato.form');
